(define-module (sfl packages sflvault)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system python))

;;; Pycrypto is abandoned upstream, no longer included in Guix (see:
;;; https://github.com/savoirfairelinux/sflvault/issues/55).  This
;;; package is still in the pinned
;;; 9ed65e6af77893b658a7159b091b5002892c2f95, but the package-cache
;;; derivation computed at 'guix pull' time fails without it (see:
;;; https://issues.guix.gnu.org/63276).
(define-public python-pycrypto
  (package
    (name "python-pycrypto")
    (version "2.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pycrypto" version))
       (patches (search-patches "python-pycrypto-CVE-2013-7459.patch"
                                "python-pycrypto-time-clock.patch"))
       (sha256
        (base32
         "0g0ayql5b9mkjam8hym6zyg6bv77lbh66rv1fyvgqb17kfc1xkpj"))))
    (build-system python-build-system)
    (inputs
     (list python gmp))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'set-build-env
           ;; pycrypto runs an autoconf configure script behind the scenes
           (lambda _ (setenv "CONFIG_SHELL" (which "bash")) #t)))))
    (home-page "https://www.dlitz.net/software/pycrypto/")
    (synopsis "Cryptographic modules for Python")
    (description
     "Pycrypto is a collection of both secure hash functions (such as SHA256
and RIPEMD160), and various encryption algorithms (AES, DES, RSA, ElGamal,
etc.).  The package is structured to make adding new modules easy.")
    (license license:public-domain)))

;;; python-keyring > 1.6.1 API has changed, which breaks 'sflvault
;;; wallet' (see:
;;; https://github.com/savoirfairelinux/sflvault/issues/51).
(define-public python-keyring-1.6
  (package
    (inherit python-keyring)
    (version "1.6.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jaraco/keyring")
             (commit "ee6fb560a3ecd17441b33a968a9243a136eb0018")))
       (file-name (git-file-name "python-keyring" version))
       (sha256
        (base32
         "1qgn1fwa3w5n5vx3nrqgxv23gqac4m9ypqk5rni4kawxax02szhx"))))
    (native-inputs '())
    (arguments
     `(#:tests? #f                  ;missing dependencies such as 'fs'
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'invoke-2to3
                    (lambda _
                      (substitute* "setup.py"
                        (("use_2to3=True,")
                         ""))
                      (invoke "2to3" "-w" ".")))
                  (add-after 'unpack 'replace-obsolete-calls
                    (lambda _
                      (substitute* (find-files "." "\\.py$")
                        (("base64.decodestring")
                         "base64.decodebytes")))))))))

(define %sflvault-commit "120617c1e01a42b97c36beef2f57c1797e75a75e")

(define* (sflvault-version #:key (revision "2"))
  (git-version "0.9.2" revision %sflvault-commit))

(define %sflvault-source
  (origin
    (method git-fetch)
    (uri (git-reference
          (url "https://github.com/savoirfairelinux/sflvault/")
          (commit %sflvault-commit)))
    (file-name (git-file-name "sflvault" (sflvault-version)))
    (sha256
     (base32
      "0a0yh15qm0vc9i6ad6yhpdbbvnr58iq71gi6b9fal0aa6fb6fszc"))))

(define-public python-sflvault-common
  (package
    (name "python-sflvault-common")
    (version (sflvault-version))
    (source %sflvault-source)
    (build-system python-build-system)
    (arguments
     `(#:tests? #f                      ;no test suite
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'chdir
                    (lambda _
                      (chdir "common"))))))
    (native-inputs (list python-wheel))
    (propagated-inputs (list python-pycrypto))
    (home-page "https://www.sflvault.org")
    (synopsis "Network credentials store and authentication manager library")
    (description "This package is a Python library that contains code
common to the SFLvault server and its clients.")
    (license license:gpl3+)))

(define-public sflvault-client
  (package
    (name "sflvault-client")
    (version (sflvault-version))
    (source %sflvault-source)
    (build-system python-build-system)
    (arguments
     `(#:tests? #f                      ;the test suite fails
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'chdir
                    (lambda _
                      (chdir "client"))))))
    (native-inputs (list python-wheel))
    (inputs
     (list python-decorator
           python-keyring-1.6
           python-pexpect
           python-pycrypto
           python-sflvault-common
           python-urwid))
    (home-page "https://www.sflvault.org")
    (synopsis "Network credentials store and authentication manager client")
    (description "This package is the client for use with an SFLvault
server.  SFLvault allows to cryptographically store and organize
passwords for different machines and services.")
    (license license:gpl3+)))
