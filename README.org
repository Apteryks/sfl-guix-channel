* SFL Guix Channel

This channel provides the =sflvault-client= package.

To use it, add the following channel configuration to your
=~/.config/guix/channels.scm= file:

#+BEGIN_SRC scheme
  (cons (channel
	 (name 'sfl-packages)
	 (url "https://gitlab.com/Apteryks/sfl-guix-channel"))
	%default-channels)
#+END_SRC

The ~sflvault-client~ package currently doesn't build using the latest
Guix, so an inferior must be used via a ~manifest.scm~ file, e.g.:

#+BEGIN_SRC scheme
  (use-modules (guix packages)
               (guix channels)
               (guix inferior)
               (guix profiles)
               (srfi srfi-1))

    ;;; An inferior for a working sflvault-client package (made
    ;;; necessary as it cannot currently be built on a Python version
    ;;; newer than 3.9.9, due to requiring legacy dependencies).
  (define sflvault-inferior
    (inferior-for-channels
     (list (channel
            (name 'guix)
            (url "https://git.savannah.gnu.org/git/guix.git")
            (commit "9ed65e6af77893b658a7159b091b5002892c2f95"))
           (channel
            (name 'sfl-guix-channels)
            (url "https://gitlab.com/Apteryks/sfl-guix-channel")
            (commit "a8d38fd765df7c302badaadef96238c7db7846b7

  (packages->manifest
   (list
    ;; List other package objects you'd like to install here.
    (first (lookup-inferior-packages sflvault-inferior
                                     "sflvault-client"))))
#+END_SRC

Then run ~guix pull~ to refresh the available packages and finally
~guix package -m manifest.scm~ to install the SFLVault client along
any other packages you have listed in your ~manifest.scm~ file.

For more information regarding Guix channels, refer to "info
'(guix)Channels'"
([[https://guix.gnu.org/manual/en/html_node/Channels.html]]).

** Contributing to this channel

To test building changes to the channel, assuming you have Guix
installed and a checkout of this channel at =~/src/sfl-guix-channel=,
you can run:

#+begin_example
guix time-machine --commit=9ed65e6af77893b658a7159b091b5002892c2f95 \
  -- build -L ~/src/sfl-guix-channel sflvault-client
#+end_example
